<?php

final class Users
{
    public  $countGroup = 2;

    private $users      =
        [
            'user 1',
            'user 2',
            'user 3',
            'user 4',
            'user 5',
            'user 6',
            'user 7',
            'user 8',
            'user 9',
            'user 10',
            'user 11',
            'user 12',
            'user 13',
            'user 14',
            'user 15',
            'user 16',
            'user 17',
            'user 18',
            'user 19',
            'user 20',
        ];

    /**
     * @return array
     * @throws Exception
     */
    public function run(): array
    {
        $usersGroup = [];

        $countGroup = $this->countGroup;
        $counUsers  = count($this->users);
        $limitGroup = ceil($counUsers / $countGroup);

        foreach ($this->users as $userId => $user) {
            $groupId = $this->findSuccessGroupId($usersGroup, $countGroup, $limitGroup);

            $usersGroup[$groupId][] = $user;
        }

        return $usersGroup;
    }

    /**
     * @param array $usersGroup
     * @param int   $groupId
     * @param int   $limitGroup
     *
     * @return bool
     */
    private function checkLimitGroup(array $usersGroup, int $groupId, int $limitGroup): bool
    {
        return (
            !empty($usersGroup[$groupId])
            and
            count($usersGroup[$groupId]) >= $limitGroup
        );
    }

    /**
     * @param array $usersGroup
     * @param int   $countGroup
     * @param int   $limitGroup
     *
     * @return int
     * @throws Exception
     */
    private function findSuccessGroupId(array $usersGroup, int $countGroup, int $limitGroup): int
    {
        $groupId         = random_int(1, $countGroup);
        $checkLimitGroup = $this->checkLimitGroup($usersGroup, $groupId, $limitGroup);

        if ($checkLimitGroup) {
            while ($checkLimitGroup) {
                $groupId         = random_int(1, $countGroup);
                $checkLimitGroup = $this->checkLimitGroup($usersGroup, $groupId, $limitGroup);
            }
        }

        return $groupId;
    }
}