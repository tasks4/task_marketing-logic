<?php

final class  Matrix
{
    /**
     * @var string
     */
    private $file;

    /**
     * @var array
     */
    private $data = [];

    /**
     * @var int
     */
    public $width = 300;

    /**
     * @var int
     */
    public $height = 300;

    /**
     * Matrix constructor.
     *
     * @param $file
     */
    public function __construct($file)
    {
        $this->file = $file;
        $this->data = $this->receiveData();
    }

    public function run(): void
    {
        $fp         = fopen(__DIR__ . '/result_task_1.csv', 'w');
        $sizeMatrix = $this->width * $this->height;
        $cell       = 1;
        $row        = 1;

        while ($cell <= $sizeMatrix) {
            $lists[$row][] = $this->data[$cell] ?? 0;

            if ($cell % $this->width === 0) {
                fputcsv($fp, $lists[$row++]);
            }

            $cell++;
        }

        fclose($fp);
    }

    /**
     * @return array
     */
    private function receiveData(): array
    {
        $data = [];

        if (($h = fopen($this->file, "r")) !== false) {
            while (($item = fgetcsv($h, 1000000, "|")) !== false) {
                $cell  = $item[0];
                $value = $item[1];

                $data[$cell] = $value;
            }

            fclose($h);
        }

        return $data;
    }
}